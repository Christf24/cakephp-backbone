<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */

if (!Configure::read('debug')):
	throw new NotFoundException();
endif;

App::uses('Debugger', 'Utility');
?>
<h2><?php echo __d('laptop_list', 'Available Laptops'); ?></h2>

<div id="controls">
	<input id="name" type="text"><input id="price" type="text"><button id="add-laptop">Add Laptop</button>
</div>

<div id="laptop-list">
	<!-- <button>Click to List</button> -->
</div>

<script type="text/template" id="item-template">
			<div class="view">
				<label><%- name %></label> - <label><%- price %></label>
				<button class="destroy"></button>
			</div>
			<input class="edit" value="<%- name %>">
		</script>