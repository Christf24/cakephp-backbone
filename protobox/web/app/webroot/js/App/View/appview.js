/**
 * App View
 *
 * { @link  http://scaleyourcode.com/interviews/interview/2 }
 * @author Christophe Limpalair <christf24@gmail.com>
 */

// set global app
app = window.app || {};

"use strict";

app.AppView = Backbone.View.extend({
	el: "#content",

	events: {
		"click #add-laptop" : "addNew"
	},

	initialize: function () {
		this.$input = this.$("#add-laptop");
		this.$list = this.$("#laptop-list");

		this.listenTo(app.LaptopCollection, 'add', this.addLaptop);
		this.listenTo(app.LaptopCollection, 'all', this.render);

		// app.LaptopCollection.fetch({reset: true});
	},

	addLaptop: function (latop) {
		var view = new app.LaptopView({ model: laptop });
		// this.$list.append(view.render().el);
	}
});

// (function($){

// 	app.ListView = Backbone.View.extend({
// 		el: '#laptop-list',
// 		initialize: function() {
// 			this.listenTo(app.laptops, "add", this.add);
// 			this.listenTo(app.laptops, "edit", this.edit);
// 			this.render();
// 		},
// 		laptopListTemplate: _.template(
// 			// "<p>" + app.laptops.get("name") + " - $" + app.laptops.get("price") + "</p>"
// 		),
// 		render: function() {
// 			this.$el.append(this.laptopListTemplate);
// 		},
// 		add: function(laptop) {
// 			var view = new app.L
// 		}
// 	});

// })(jQuery);