/**
 * Laptops List View
 *
 * { @link  http://scaleyourcode.com/interviews/interview/2 }
 * @author Christophe Limpalair <christf24@gmail.com>
 */

// set global app
app = window.app || {};

"use strict";

app.LaptopView = Backbone.View.extend({
	tagName: 'li',

	// Cache template function for a single laptop
	template: _.template($("#item-template").html()),

	// DOM events specific to a laptop
	events: {
		"click .destroy": "clear",
		"blur .edit": "close"
	},

	initialize: function () {
		// this.listenTo(this.model, 'change', this.render);
		// this.listenTo(this.model, 'destroy', this.remove);
	}
})