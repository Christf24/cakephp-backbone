/**
 * Laptops Collection
 *
 * { @link  http://scaleyourcode.com/interviews/interview/2 }
 * @author Christophe Limpalair <christf24@gmail.com>
 */

// set global app
app = window.app || {};

"use strict";

var Collection = Backbone.Collection.extend({
  	// Reference to this collection's model
  	mode: app.LaptopModel,

  	// Save all of the todo items under the `"todos"` namespace.
	// localStorage: new Backbone.LocalStorage('todos-backbone')
});

app.LaptopCollection = new Collection();
