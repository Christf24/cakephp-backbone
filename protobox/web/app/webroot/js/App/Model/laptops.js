/**
 * Laptops Model
 *
 * { @link  http://scaleyourcode.com/interviews/interview/2 }
 * @author Christophe Limpalair <christf24@gmail.com>
 */
// set global app
app = window.app || {};

"use strict";

// Extend Backbone's Model feature
app.LaptopModel = Backbone.Model.extend({
	// initialize: function(options) {
		// initialize this model with event listeners
		// this.on("change:name", function (model) {
		// 	console.log("Laptop is now called " + model.get("name"));
		// },
		// this.on("change:age", function(model) {
		// 	console.log("Laptop now costs " + model.get("price"));
		// })
		// );
	// },
	// This makes sure that every laptop created has a name and price
	defaults: {
		name: '',
		price: 0
	}
});