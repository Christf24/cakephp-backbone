/**
 * Example Backbone Application
 *
 * This was written after my interview with Michael Oostdyk
 * to apply what I learned from the interview research,
 * the interview itself, and my own fiddling around.
 *
 * Feel free to use this for anything you'd like!
 * 
 * So I guess I should put some kind of disclaimer here?
 * I'm not responsible for anything except expanding your knowledge
 * and helping you build something amazing! Enjoy!
 * 
 * { @link  http://scaleyourcode.com/interviews/interview/2 }
 * @author  Christophe Limpalair <christf24@gmail.com>
 */

// set global app
app = window.app || {};

// more sensitive to error reporting
"use strict";

// load the app!
new app.AppView();
